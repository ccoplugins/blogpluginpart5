package com.hokona.cco.blogpluginpart5.dao;


import com.hokona.cco.blogpluginpart5.dto.AdditionalInfoDto;
import com.sap.scco.ap.pos.dao.CDBSession;
import com.sap.scco.ap.pos.dao.CDBSessionFactory;
import com.sap.scco.util.logging.Logger;
import org.apache.commons.lang.StringUtils;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * robertzieschang 12.11.20
 * <p>
 * Simple DAO class to make data in our user defined table easily accessible
 **/
public class AdditionalInfoDao {

    // use the logger from cco
    private static final Logger logger = Logger.getLogger(AdditionalInfoDao.class);

    private static final String TABLE_NAME = "HOK_ADDITIONALINFO";

    private static final String QUERY_CREATE_TABLE = "CREATE TABLE " + TABLE_NAME + " ("
            + "CUSTOMERID varchar(20) not null PRIMARY KEY,"
            + "ADDFLD1 varchar(254) not null, "
            + "ADDFLD2 INT not null, "
            + "ADDFLD3 decimal(18,4) not null, "
            + "ADDFLD4 varchar(254))";

    private static final String QUERY_INSERT_ROW = "INSERT INTO " + TABLE_NAME + " VALUES(?,?,?,?,?)";

    private static final String QUERY_UPDATE_ROW = "UPDATE " + TABLE_NAME
            + " SET ADDFLD1 =?2 , ADDFLD2 =?3, ADDFLD3 = ?4, ADDFLD4 = ?5 WHERE CUSTOMERID = ?1";

    private static final String QUERY_FIND_ALL = "SELECT CUSTOMERID, ADDFLD1, ADDFLD2, ADDFLD3, ADDFLD4 FROM " + TABLE_NAME;

    private static final String QUERY_FIND_ONE = QUERY_FIND_ALL + " WHERE CUSTOMERID = ?";

    private static final String QUERY_DROP_ONE = "DELETE FROM " + TABLE_NAME + " WHERE CUSTOMERID = ?";

    private static AdditionalInfoDao instance;

    public static synchronized AdditionalInfoDao getInstance() {
        if (instance == null) {
            instance = new AdditionalInfoDao();
        }
        return instance;
    }

    private AdditionalInfoDao() {

    }

    public void setupTable() {
        CDBSession session = CDBSessionFactory.instance.createSession();
        try {
            session.beginTransaction();

            EntityManager em = session.getEM();

            Query q = em.createNativeQuery(QUERY_CREATE_TABLE);
            q.executeUpdate();

            session.commitTransaction();
            logger.info("Created Table" + TABLE_NAME);
        } catch (Exception e) {
            session.rollbackDBSession();
            logger.info("Table " + TABLE_NAME + " already exists or another db related error occured");
        } finally {
            session.closeDBSession();
        }
    }

    private void save(AdditionalInfoDto dto, boolean isAlreadyInDB) {
        CDBSession session = CDBSessionFactory.instance.createSession();
        String query = isAlreadyInDB ? QUERY_UPDATE_ROW : QUERY_INSERT_ROW;

        try {
            session.beginTransaction();
            EntityManager em = session.getEM();

            Query q = em.createNativeQuery(query);
            q.setParameter(1, dto.getCustomerId());
            q.setParameter(2, dto.getAddFld1());
            q.setParameter(3, dto.getAddFld2());
            q.setParameter(4, dto.getAddFld3().doubleValue());
            q.setParameter(5, dto.getAddFld4());

            q.executeUpdate();
            session.commitTransaction();

        } catch (Exception e) {
            session.rollbackDBSession();
            logger.info("Could not create LoyaltyFactor");
            logger.info(e.getLocalizedMessage());
        } finally {
            session.closeDBSession();
        }
    }

    public List<AdditionalInfoDto> findAll() {
        CDBSession session = CDBSessionFactory.instance.createSession();
        ArrayList<AdditionalInfoDto> resultList = new ArrayList<AdditionalInfoDto>();

        try {
            session.beginTransaction();
            EntityManager em = session.getEM();

            Query q = em.createNativeQuery(QUERY_FIND_ALL);
            @SuppressWarnings("unchecked")
            List<Object[]> results = q.getResultList();

            if (results.isEmpty()) {
                // return an empty list rather than null
                return resultList;
            }

            for (Object[] resultRow : results) {
                resultList.add(new AdditionalInfoDto((String) resultRow[0], (String) resultRow[1], (int) resultRow[2], (BigDecimal) resultRow[3], (String) resultRow[4]));
            }

        } catch (Exception e) {
            logger.info("Error while getting results from table " + TABLE_NAME);

        } finally {
            session.closeDBSession();

        }
        return resultList;
    }

    public AdditionalInfoDto findOne(String customerId) {
        CDBSession session = CDBSessionFactory.instance.createSession();
        AdditionalInfoDto additionalInfoDto = new AdditionalInfoDto();

        try {

            EntityManager em = session.getEM();
            Query q = em.createNativeQuery(QUERY_FIND_ONE);
            q.setParameter(1, customerId);

            @SuppressWarnings("unchecked")
            List<Object[]> results = q.getResultList();

            if (results.isEmpty()) {
                // return empty dto
                return additionalInfoDto;
            }

            additionalInfoDto.setCustomerId((String) results.get(0)[0]);
            additionalInfoDto.setAddFld1((String) results.get(0)[1]);
            additionalInfoDto.setAddFld2((int) results.get(0)[2]);
            additionalInfoDto.setAddFld3((BigDecimal) results.get(0)[3]);
            additionalInfoDto.setAddFld4((String) results.get(0)[4]);


        } catch (Exception e) {
            logger.info("Error while getting " + customerId + " from table " + TABLE_NAME);
        } finally {
            session.closeDBSession();
        }
        return additionalInfoDto;
    }

    public void save(AdditionalInfoDto dto) {
        AdditionalInfoDto dtoInDb = this.findOne(dto.getCustomerId());
        boolean isAlreadyInDb = StringUtils.equals(dto.getCustomerId(), dtoInDb.getCustomerId());
        logger.info("Trying to save " + dto.toString() + " and it was in db " + isAlreadyInDb);
        this.save(dto, isAlreadyInDb);
    }

}
