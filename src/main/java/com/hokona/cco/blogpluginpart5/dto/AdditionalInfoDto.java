package com.hokona.cco.blogpluginpart5.dto;

import java.math.BigDecimal;

/**
 * robertzieschang 12.11.20
 **/
public class AdditionalInfoDto {

    private String customerId;

    private String addFld1;

    private int addFld2;

    private BigDecimal addFld3;

    private String addFld4;

    public AdditionalInfoDto() {
        
    }

    public AdditionalInfoDto(String customerId, String addFld1, int addFld2, BigDecimal addFld3, String addFld4) {
        this.customerId = customerId;
        this.addFld1 = addFld1;
        this.addFld2 = addFld2;
        this.addFld3 = addFld3;
        this.addFld4 = addFld4;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getAddFld1() {
        return addFld1;
    }

    public void setAddFld1(String addFld1) {
        this.addFld1 = addFld1;
    }

    public int getAddFld2() {
        return addFld2;
    }

    public void setAddFld2(int addFld2) {
        this.addFld2 = addFld2;
    }

    public BigDecimal getAddFld3() {
        return addFld3;
    }

    public void setAddFld3(BigDecimal addFld3) {
        this.addFld3 = addFld3;
    }

    public String getAddFld4() {
        return addFld4;
    }

    public void setAddFld4(String addFld4) {
        this.addFld4 = addFld4;
    }

    @Override
    public String toString() {
        return "{"
                + " 'receiptId' : " + this.getCustomerId() + " ,"
                + " 'addFld1' : " + this.getAddFld1() + " ,"
                + " 'addFld2' : " + this.getAddFld2() + " ,"
                + " 'addFld3' : " + this.getAddFld3() + " ,"
                + " 'addFld4' : " + this.getAddFld4()
                + "}";
    }
}
