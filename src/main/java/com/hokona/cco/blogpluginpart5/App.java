package com.hokona.cco.blogpluginpart5;

import com.hokona.cco.blogpluginpart5.dao.AdditionalInfoDao;
import com.hokona.cco.blogpluginpart5.dto.AdditionalInfoDto;
import com.hokona.cco.blogpluginpart5.handler.BackendEventHandler;
import com.hokona.cco.blogpluginpart5.print.AdditionalInfoPrintJobBuilder;
import com.sap.scco.ap.plugin.BasePlugin;
import com.sap.scco.ap.plugin.annotation.PluginAt;
import com.sap.scco.ap.plugin.annotation.ui.CSSInject;
import com.sap.scco.ap.plugin.annotation.ui.JSInject;
import com.sap.scco.ap.pos.dao.CDBSession;
import com.sap.scco.ap.pos.dao.CDBSessionFactory;
import com.sap.scco.ap.pos.entity.ReceiptEntity;
import com.sap.scco.ap.pos.printer.PrintEngine;
import com.sap.scco.ap.pos.printer.PrintJob;
import com.sap.scco.ap.pos.printer.PrintJobManager;
import com.sap.scco.ap.pos.service.PrintReceiptPosService;
import com.sap.scco.util.conf.CConfig;
import com.sap.scco.util.exception.WrongUsageException;
import com.sap.scco.util.logging.Logger;
import freemarker.template.TemplateException;
import org.apache.commons.io.IOUtils;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import java.io.*;
import java.util.List;

/**
 * robertzieschang 12.11.20
 * <p>
 * Base class of the plugin.
 **/
public class App extends BasePlugin {
    // get the logger to log stuff to the logfiles & console
    private static final Logger log = Logger.getLogger(App.class);

    @Override
    public String getId() {
        return PluginPropertiesConstants.PLUGIN_ID;
    }

    @Override
    public String getName() {
        return PluginPropertiesConstants.PLUGIN_NAME;
    }

    @Override
    public String getVersion() {
        return this.getClass().getPackage().getImplementationVersion();
    }

    @Override
    public void startup() {
        AdditionalInfoDao.getInstance().setupTable();
        BackendEventHandler.getInstance().registerEventHandler();
        addPrintJobBuilder();
        deployPrintTemplate();
        super.startup();
    }

    @PluginAt(pluginClass = PrintReceiptPosService.class, method = "printReceipt", where = PluginAt.POSITION.AFTER)
    public Object onReceiptPrinted(Object proxy, Object[] args, Object ret, StackTraceElement caller) {
        ReceiptEntity receipt = (ReceiptEntity) args[0];
        if (null != receipt.getBusinessPartner()) {
            AdditionalInfoDto addInfo = AdditionalInfoDao.getInstance().findOne(receipt.getBusinessPartner().getExternalId());
            if (addInfo.getCustomerId() != null) {
                triggerPrint(addInfo, receipt.getCountPrintouts() > 1);
            }
        }
        return ret;
    }

    private void triggerPrint(AdditionalInfoDto additionalInfoDto, boolean isReprint) {
        try (CDBSession session = CDBSessionFactory.instance.createSession()) {
            List<PrintJob<?>> printJobs = PrintJobManager.INSTANCE.createPrintJobs(additionalInfoDto, session, isReprint);
            PrintEngine.INSTANCE.print(printJobs, false);
        } catch (IOException | WrongUsageException | ParserConfigurationException | SAXException | TemplateException | TransformerException | InterruptedException e) {
            e.printStackTrace();
        }

    }


    private void deployPrintTemplate() {
        try {
            String templateFileName = PluginPropertiesConstants.ADDITIONAL_INFO_PRINT_TEMPLATE + ".xsl";
            extractResource("/resources/" + templateFileName, CConfig.getPrintTemplatePath(), false);
        } catch (WrongUsageException ex) {
            log.severe(PluginPropertiesConstants.EXCEPTION_PRINT_TEMPLATE_COULD_NOT_BE_DEPLOYED);
        }
    }

    public void extractResource(String resName, String path, boolean overwrite) throws WrongUsageException {
        InputStream oIn = this.getClass().getResourceAsStream(resName);
        if (null != oIn) {
            try {
                String outName = resName.substring(resName.lastIndexOf("/"));
                String fileName = path + File.separator + outName;
                File oFl = new File(fileName);
                if (!oFl.exists() || overwrite) {
                    OutputStream oOut = new FileOutputStream(oFl);
                    IOUtils.copy(oIn, oOut);
                    oOut.close();
                }
                oIn.close();
            } catch (IOException oX) {
                throw new WrongUsageException("Error extracting: " + oX.getMessage());
            }
        } else {
            throw new WrongUsageException("Could not find resource '" + resName + "'.");
        }
    }

    private void addPrintJobBuilder() {
        PrintJobManager.INSTANCE.addPrintJobBuilders(AdditionalInfoPrintJobBuilder.getInstance());
    }


    @JSInject(targetScreen = "NGUI")
    public InputStream[] jsInject() {
        log.finest("Injecting NGUI Javascript code.");
        return new InputStream[]{this.getClass().getResourceAsStream("/resources/blogpluginpart5.js")};
    }

    @CSSInject(targetScreen = "NGUI")
    public InputStream[] cssInject() {
        log.finest("Injecting NGUI CSS code.");
        return new InputStream[]{this.getClass().getResourceAsStream("/resources/blogpluginpart5.css")};
    }
}
